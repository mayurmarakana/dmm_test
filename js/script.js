
$(document).ready(function($) {
  //Top Nav Tabs
  $('.tabs').each(function(intex, element) {
      current_tabs = $(this);
      $(this).prepend('<div class="tab-nav line"></div>');
      var tab_buttons = $(element).find('.tab-label');
      $(this).children('.tab-nav').prepend(tab_buttons);
      $(this).children('.tab-item').each(function(i) {
          $(this).attr("id", "tab-" + (i + 1));
      });
      $(".tab-nav").each(function() {
          $(this).children().each(function(i) {
              $(this).attr("href", "#tab-" + (i + 1));
          });
      });
      $(this).find(".tab-nav a").click(function(event) {
          $(this).parent().children().removeClass("active-btn");
          $(this).addClass("active-btn");
          var tab = $(this).attr("href");
          $(this).parent().parent().find(".tab-item").not(tab).css("display", "none");
          $(this).parent().parent().find(tab).fadeIn();
          return false;
      });
	  
	  $('#top_nav_bar li').each(function() {
		$(this).unbind('mouseover').bind('mouseover', function() {
			var iHeight = parseInt($(this).find('div').height());
			$(this).find('div').css('background-position-y', iHeight + 1);
			$(this).addClass('whiteBg');
		});
		$(this).unbind('mouseout').bind('mouseout', function() {
			var iHeight = parseInt($(this).height());
			$(this).find('div').css('background-position-y', 0);
			$(this).removeClass('whiteBg');
		});
   });
  });

  //Mobile navigation  
  $('.nav-text').click(function() { 
    $('.top-nav > ul').toggleClass('show-menu', 'slow');
  }); 
	
});